-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: bd_petshop
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animal`
--

DROP TABLE IF EXISTS `animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `id_dono_animal` int(11) NOT NULL,
  `nome_animal` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_animal`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animal`
--

LOCK TABLES `animal` WRITE;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` VALUES (1,1,'Belinha'),(2,1,'Brutus'),(3,2,'Fred'),(4,2,'Pluto');
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banho`
--

DROP TABLE IF EXISTS `banho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banho` (
  `id_banho` int(11) NOT NULL AUTO_INCREMENT,
  `id_dono_animal` int(11) DEFAULT NULL,
  `id_animal` int(11) DEFAULT NULL,
  `data_banho` date DEFAULT NULL,
  `hora_banho` time DEFAULT NULL,
  `valor_banho` decimal(10,2) DEFAULT NULL,
  `foi_pago_banho` int(11) DEFAULT NULL,
  `observacao_banho` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_banho`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banho`
--

LOCK TABLES `banho` WRITE;
/*!40000 ALTER TABLE `banho` DISABLE KEYS */;
INSERT INTO `banho` VALUES (1,1,1,'2017-09-13','10:00:00',50.00,0,'Tosar tudo.'),(2,1,2,'2017-09-13','10:30:00',50.00,0,'Tosar tudo.'),(3,2,1,'2017-09-13','11:00:00',50.00,0,'Tosar tudo.'),(4,2,2,'2017-09-13','11:30:00',50.00,0,'Tosar tudo.');
/*!40000 ALTER TABLE `banho` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dono_animal`
--

DROP TABLE IF EXISTS `dono_animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dono_animal` (
  `id_dono_animal` int(11) NOT NULL AUTO_INCREMENT,
  `nome_dono_animal` varchar(45) DEFAULT NULL,
  `cpf_dono_animal` varchar(11) DEFAULT NULL,
  `telefone_dono_animal` varchar(10) DEFAULT NULL,
  `endereco_dono_animal` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_dono_animal`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dono_animal`
--

LOCK TABLES `dono_animal` WRITE;
/*!40000 ALTER TABLE `dono_animal` DISABLE KEYS */;
INSERT INTO `dono_animal` VALUES (1,'Danilo','34525287829','1636233966','Rua Doutor José Guimarães, 198'),(2,'Fernando','99999999999','1699999999','Rua Cesar Vergueiro, 677');
/*!40000 ALTER TABLE `dono_animal` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-13 10:50:43
